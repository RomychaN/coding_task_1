// eslint-disable-next-line no-undef
const fs = require('fs');
const path = require('path');

const dirData = process.env.npm_config_folder || './data';
const filterValue = process.env.npm_config_filter || 'none';
const exceptionFiles = process.env.npm_config_files || [];

class FilterManager {
  constructor() {
    this._filters = [];
  }

  addFilter(filter) {
    this._filters = [...this._filters, filter];
  }

  getFilter(name) {
    return this._filters.find((strategy) => strategy._name === name);
  }
}

class Filter {
  constructor(name, isValidate) {
    this._name = name;
    this._isValidate = isValidate;
  }

  isExceptionFile(name) {
    return this._isValidate(name);
  }
}

const filterManager = new FilterManager();

const filterExt = new Filter(
    'ext',
    (ext) => !exceptionFiles.includes(path.extname(ext))
);
const filterName = new Filter('name', (name) =>
  exceptionFiles.includes(path.basename(name, path.extname(name)))
);
const withoutFilter = new Filter('none', () => true);

filterManager.addFilter(filterExt);
filterManager.addFilter(filterName);
filterManager.addFilter(withoutFilter);

const filter = filterManager.getFilter(filterValue);

function entropy(fileContent) {
  const contentLength = fileContent.length;

  const frequencies = Array.from(fileContent).reduce(
      (frequency, current) =>
        (frequency[current] = (frequency[current] || 0) + 1) && frequency,
      {}
  );

  return Object.values(frequencies).reduce(
      (sum, current) =>
        sum - (current / contentLength) * Math.log2(current / contentLength),
      0
  );
}

function filesProcessing(dir, filesTemp = []) {
  const files = fs.readdirSync(dir);

  files.forEach((file) => {
    const name = `${dir}/${file}`;

    if (fs.statSync(name).isDirectory()) {
      filesProcessing(name, filesTemp);
    } else {
      if (filter.isExceptionFile(name)) {
        filesTemp.push(entropy(fs.readFileSync(name, 'utf8')));
      }
    }
  });

  return filesTemp;
}

const arrayEntropy = filesProcessing(dirData);

function countGeneralEntropy(result) {
  return result.reduce((sum, current) => sum + current, 0);
}

function averageEntropy(array) {
  return countGeneralEntropy(array) / array.length;
}

// eslint-disable-next-line max-len
console.log(`Общая энтропия = ${countGeneralEntropy(arrayEntropy)}`); // 2585.7967664582347
// eslint-disable-next-line max-len
console.log(`Средняя энтропия = ${averageEntropy(arrayEntropy)}`); // 4.788512530478212
